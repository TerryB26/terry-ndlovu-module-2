/*Create an array to store all the winning apps of the MTN Business App of the Year Awards since 2012; 
a) Sort and print the apps by name;  
b) Print the winning app of 2017 and the winning app of 2018.; 
c) the Print total number of apps from the array.
*/

void main(List<String> arguments) 
{
  // ignore: unused_local_variable
  var winningApps = ['FNB Banking App - 2012' , 'SnapScan - 2013' , 'Live Inspect - 2014' ,
                     'WumDrop - 2015' , 'Domestly - 2016' , 'Standard Bank Shyft - 2017', 
                     'Khula - 2018' , 'Naked Insurance - 2019' , 'EasyEquities - 2020' , 'Amabani - 2021'];
  
  

  //a) Sort and Print the apps by name 
  // ignore: prefer_adjacent_string_concatenation
  print('a) Sort and Print the apps by name\n' + '___________________________________\n');
  winningApps.sort();
  for (var i = 0; i < winningApps.length; i++) 
  {
    print(winningApps[i]);
  }
  print('___________________________________\n');

  //b) Print the winning app of 2017 and the winning app of 2018.; 
  // ignore: prefer_adjacent_string_concatenation
  print('b) Print the winning app of 2017 and the winning app of 2018.\n'  + '__________________________________________________________\n');
  print('The winning apps for 2017 amd 2018 were: ');
  
  for (var i = 0; i < winningApps.length; i++) 
  {
    if (winningApps[i].contains('2017')) 
    {
      print(winningApps[i]);
    } 

    if (winningApps[i].contains('2018')) 
    {
      print(winningApps[i]);
    } 
  }
   print('___________________________________\n');
   
//c) the Print total number of apps from the array
// ignore: prefer_adjacent_string_concatenation
print('c) the Print total number of apps from the array\n'  + '_____________________________________________________\n');
print('The total number of apps in the array is : ' + (winningApps.length).toString());
   



  
}
