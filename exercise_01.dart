//Write a basic program that stores and then prints the following data: Your name, favorite app, and city;
void main(List<String> args) 
{
  String firstName,lastName,favoriteApp,city;
  firstName = 'Terry';
  lastName = 'Ndlovu';
  favoriteApp = 'Telegram';
  city = 'Pretoria';

  print('I am '+firstName + ' ' + lastName + '.');
  print('My favorite app is ' + favoriteApp + '.');
  print('My city is '+city + '.');
}