// Create a class and 
// a) then use an object to print the name of the app, sector/category, developer, and the year it won MTN Business App of the Year Awards. 
// b) Create a function inside the class, transform the app name to all capital letters and then print the output.

// ignore: unused_import
import 'dart:ffi';

class AppInfo 
{
  late String appName;
  late String appCategory;
  late String appDeveloper;
  late int year;

    showOutput()
  {
    print('The name of the app is:' + appName);
    print('The app is on the ' + appCategory + ' category.');
    print('It was developed by: '+appDeveloper );
    print('It won the app of the year in ' + year.toString());

  }

  appAllCaps(String name)
  {
    print(name.toUpperCase());
  }

}

void main(List<String> arguments) {
  
  AppInfo app = AppInfo();
  app.appName = ' Ambani Africa';
  app.appCategory = 'Best Gaming Solution, Best Educational and Best South African Solution';
  app.appDeveloper = 'Mukundi Lambani';
  app.year = 2021;
  app.showOutput();

  print(' ');

  app.appAllCaps('Ambani Africa');
  
  
}
